import cv2
import time
import matplotlib.pyplot as plt
import numpy


if __name__ == "__main__":
    print("detect start! ", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))  #print current time
    im = cv2.imread("in.jpg")  #load image
    im_gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY) / 255.  #convert image into gray
    print("show cvt pic:")
    #plt.imshow(im_gray)  #show image
    #plt.show()
    
    f = numpy.load("weights.npz")
    for ii in numpy.load("weights.npz"):
        #print(type(f[ii]))
        if type(f[ii]) != numpy.ndarray:
            f.files.pop(f.files.index(ii))
        #print(f.files.pop(f.files.index(ii)))
    print(f.files) #['arr_1', 'arr_0', 'arr_3', 'arr_2', 'arr_5', 'arr_4', 'arr_7', 'arr_6', 'arr_9', 'arr_8']
    print(sorted(f.files, key=lambda s: int(s[-1]))) # ['arr_0', 'arr_1', 'arr_2', 'arr_3', 'arr_4', 'arr_5', 'arr_6', 'arr_7', 'arr_8', 'arr_9']

    param_vals = [f[n] for n in sorted(f.files, key=lambda s: int(s[-1]))]
    #print(param_vals)
    for n in sorted(f.files, key=lambda s: int(s[-1])):
        print(n)
# Using opencv to show an image
    """img = cv2.imread('in.jpg',0)
    cv2.imshow('image',img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()"""
