#-*-coding: utf-8-*-  
from skimage import img_as_ubyte
from skimage import measure,color
from skimage import io,data

import cv2
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    '''Step1: 輸入一張圖'''
    original_image = cv2.imread('in_H.jpg')
    original_image = cv2.resize(original_image, (400, 400*original_image.shape[0]/original_image.shape[1])) # 等比例縮小
    '''Step2: 抓出rgb'''
    R = original_image[:,:,0]
    G = original_image[:,:,1]
    B = original_image[:,:,2]
    #cv2.imshow('R', R)
    #cv2.waitKey(0)
    '''Step3: 利用方程式把image轉成灰階'''
    Gray_image = R*0.299 + G*0.587 + B*0.114 
    '''把他scale到0~1'''
    Gray_image_scale = Gray_image /255
    #cv2.imshow('Gray_image_scale', Gray_image_scale)
    #cv2.waitKey(0)
    Gray_image_Height = Gray_image.shape[0]
    Gray_image_Width = Gray_image.shape[1]
    '''Step4: 畫出histogram'''
    histogram = np.zeros(256)
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            histogram[int(Gray_image[i][j])] = histogram[int(Gray_image[i][j])]+1

    """
    bins = np.arange(0, 256, 1) 
    k = plt.bar(bins, histogram, .5)
    plt.title('(0.299*R)+(0.587*G)+(0.114*B) histogram')
    plt.xlabel('Gray scale')
    plt.show()
    """
    
    '''Step5: histogram equalisation'''
    dst=img_as_ubyte(Gray_image/255)
    Gray_image_histogram_equalisation = cv2.equalizeHist(dst)
    
    '''Step5-1: 畫出histogram'''
    histogram_after = np.zeros(256)
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            histogram_after[int(Gray_image_histogram_equalisation[i][j])] = histogram_after[int(Gray_image_histogram_equalisation[i][j])]+1
    """
    bins = np.arange(0, 256, 1) 
    k = plt.bar(bins, histogram_after, .5)
    plt.title('(0.299*R)+(0.587*G)+(0.114*B) histogram equalisation')
    plt.xlabel('Gray scale')
    plt.show()
    """
    
    '''Step5-2: 畫出對照圖'''
    """
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    ax1.imshow(dst, plt.cm.gray, interpolation='nearest')
    ax1.axis('off')
    ax2.imshow(Gray_image_histogram_equalisation, plt.cm.gray, interpolation='nearest')
    ax2.axis('off')         
    fig.tight_layout()
    plt.show()  
    """

    '''Step6: 用sobel做邊緣偵測'''
    x = cv2.Sobel(Gray_image_histogram_equalisation, cv2.CV_16S, 1, 0)
    y = cv2.Sobel(Gray_image_histogram_equalisation, cv2.CV_16S, 0, 1)
    absX = cv2.convertScaleAbs(x)
    absY = cv2.convertScaleAbs(y)
    Sobel_image = cv2.addWeighted(absX, 0.5, absY, 0.5, 0)
    #dst=io.imshow(Sobel_image)
    #io.show()
    
    '''Step7: 把sobel image做二值化'''
    ImageThreshold = np.zeros([Gray_image_Height, Gray_image_Width])
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            if Sobel_image[i][j] <= 127:
                ImageThreshold[i][j] = 0
            else:
                ImageThreshold[i][j] = 255

    ImageThreshold=img_as_ubyte(ImageThreshold/255)
    dst=io.imshow(ImageThreshold)
    io.show()
    
    
    #openingimg = cv2.morphologyEx(grayimg, cv2.MORPH_OPEN, kernel)             
    #element1 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
    '''Step8: 開運算'''
    """element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    openingimg = cv2.morphologyEx(ImageThreshold, cv2.MORPH_OPEN, element2)       
    dst=io.imshow(openingimg)
    io.show()"""  
    '''Step8-1: 閉運算'''
    element3 = cv2.getStructuringElement(cv2.MORPH_RECT, (4, 4))     
    closingimg = cv2.morphologyEx(ImageThreshold, cv2.MORPH_CLOSE, element3)
    dst=io.imshow(closingimg)
    io.show()   

    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    openingimg1 = cv2.morphologyEx(closingimg, cv2.MORPH_OPEN, element2)       
    dst=io.imshow(openingimg1)
    io.show() 
                 
    labels=measure.label(openingimg1,connectivity=1)  #8连通区域标记     
    #dst=io.imshow(labels)
    #io.show()           
    dst=color.label2rgb(labels)  #根据不同的标记显示不同的颜色
    #print('regions number:',labels.max()+1)  #显示连通区域块数(从0开始标记)
    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    ax1.imshow(openingimg1, plt.cm.gray, interpolation='nearest')
    ax1.axis('off')
    ax2.imshow(dst,interpolation='nearest')
    ax2.axis('off')         
    fig.tight_layout()
    plt.show()  
               

    ImageThreshold_crop = np.zeros([Gray_image_Height, Gray_image_Width])
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            if Gray_image[i][j] <= 127:
                ImageThreshold_crop[i][j] = 0
            else:
                ImageThreshold_crop[i][j] = 255
    #dst=io.imshow(closingimg)
    ##io.show()
    Ratio = []
    blocks = []
    labels=img_as_ubyte(labels)
    contours, hierarchy = cv2.findContours(labels, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        y, x = [], [] 
        for p in contour:
            #y = p
            # append() 方法用于在列表末尾添加新的对象。
            #print(p)
            y.append(p[0][0])
            x.append(p[0][1])        
        r = [np.min(y), np.min(x), np.max(y), np.max(x)] #頂點
        a = (r[2]-r[0]) * (r[3]-r[1]) #面積 長x寬
        s = (r[2]-r[0]) / (r[3]-r[1]) #長寬比 
        #print(s)
        crop_img = ImageThreshold_crop[np.min(x):np.max(x), np.min(y):np.max(y)]
        #dst=io.imshow(crop_img)
        #io.show()
        black = 0
        total = 0
        for k in range(crop_img.shape[0]):
            for m in range(crop_img.shape[1]):
                if crop_img[k][m] == 0:
                    black = black + 1
                    total = total + 1
                else:
                    total = total + 1
        if total != 0:
            if total >= 1000:
                ratio_total_black = float(black) / total
                #print(ratio_total_black,black, total)
                if ratio_total_black > 0.3:
                    #print("A")
                    if ratio_total_black < 0.4:
                        if s <= 3:
                            print("A")
        if a >= 1000:
            blocks.append([r, a, s]) 
                            #print("black=",black)
                            #print("total=",total)
                            #print("ratio=",s)
    blocks = sorted(blocks, key=lambda b: b[2])[-len(blocks):]
    
    for i in range(len(blocks)):
        blocks1 = blocks[i][0]
        cv2.rectangle(original_image, (blocks1[0], blocks1[1]), (blocks1[2], blocks1[3]), (0,255,0),2)    
        dst=io.imshow(original_image)
        io.show()
        
#     
                 
"""
crop_img = original_image[206:250, 77:147]  
crop_img=img_as_ubyte(crop_img/255)

dst=io.imshow(crop_img)
io.show()
"""      
                
                
                
                




