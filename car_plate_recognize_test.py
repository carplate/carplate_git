#-*-coding: utf-8-*-  
#!/usr/bin/python

import cv2
import numpy as np
import math

     
def dobinaryzation(img):
    max = float(img.max())
    min = float(img.min())
     
    x = max - ((max-min) / 2)
    ret, threshedimg = cv2.threshold(img, x, 255, cv2.THRESH_BINARY)
     
    return threshedimg
	
if __name__ == '__main__':
	# 读取图片
    orgimg = cv2.imread('in.jpg')
    img11 = cv2.resize(orgimg, (400, 400*img.shape[0]/img.shape[1])) # 等比例縮小
    #cv2.imshow('orgimg', orgimg)
    #cv2.waitKey(0)
    img = orgimg
    #rect, img = find_license(orgimg)
    img = cv2.resize(img, (400, 400*img.shape[0]/img.shape[1])) # 等比例縮小
    #cv2.imshow('img', img)
    #cv2.waitKey(0)
    grayimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #cv2.imshow('grayimg', grayimg)
    #cv2.waitKey(0)
    max = float(grayimg.max())
    min = float(grayimg.min())
    for i in range(grayimg.shape[0]):
        for j in range(grayimg.shape[1]):
            grayimg[i, j] = (255/(max-min))*grayimg[i,j]-(255*min)/(max-min)
    #stretchedimg = stretch(grayimg)
    r = 16
    h = w = r * 2 + 1
    kernel = np.zeros((h, w), dtype=np.uint8)
    cv2.circle(kernel, (r, r), r, 1, -1)
    #(AθB)⊕B
    # 開運算：先腐蝕，再膨脹，可以清除一些小東西(亮的)，放大區域性低亮度的區域
    # 先進行腐蝕再進行膨脹的運算就是開運算，腐蝕可以讓那些在影象外面的小點點去掉，然後把主影象膨脹回去，實現去除影象外噪聲。
    #（１）開運算能夠除去孤立的小點，毛刺和小橋，而總的位置和形狀不便。 
    #（２）開運算是一個基於幾何運算的濾波器。 
    #（３）結構元素大小的不同將導致濾波效果的不同。 
    #（４）不同的結構元素的選擇導致了不同的分割，即提取出不同的特徵。
    openingimg = cv2.morphologyEx(grayimg, cv2.MORPH_OPEN, kernel)
    #cv2.imshow('openingimg', openingimg)
    #cv2.waitKey(0) 
    #Edge(A)=A-(AθB)⊕B
    # 頂帽：原影象 - 開運算圖。突出原影象中比周圍亮的區域
    strtimg = cv2.absdiff(grayimg, openingimg)
    #cv2.imshow('strtimg', strtimg)
    #cv2.waitKey(0) 

    binaryimg = dobinaryzation(strtimg)
    #cv2.imshow('binaryimg', binaryimg)
    #cv2.waitKey(0) 
    # 使用Canny函数做边缘检测
    cannyimg = cv2.Canny(binaryimg, binaryimg.shape[0], binaryimg.shape[1])
    #cv2.imshow('cannyimg', cannyimg)
    #cv2.waitKey(0) 
    ''' 消除小区域，保留大块区域，从而定位车牌'''
    # 如果想消除图像中的噪声（即图像中的“小点”），也可以对图像先用开运算后用闭运算，不过这样也会消除一些破碎的对象。
    # 进行闭运算
    # 先膨脹，再腐蝕，可以清除小黑點
    #（1）閉運算能夠填平小湖（即小孔），彌合小裂縫，而總的位置和形狀不變。 
    #（2）閉運算是通過填充影象的凹角來濾波影象的。 
    #（3）結構元素大小的不同將導致濾波效果的不同。 
    #（4）不同結構元素的選擇導致了不同的分割。
    kernel = np.ones((5,19), np.uint8)
    closingimg = cv2.morphologyEx(cannyimg, cv2.MORPH_CLOSE, kernel)
    #cv2.imshow('closingimg', closingimg)
    #cv2.waitKey(0) 
	# 进行开运算
    openingimg = cv2.morphologyEx(closingimg, cv2.MORPH_OPEN, kernel)
    #cv2.imshow('openingimg', openingimg)
    #cv2.waitKey(0)
	# 再次进行开运算
    kernel = np.ones((11,5), np.uint8)
    openingimg = cv2.morphologyEx(openingimg, cv2.MORPH_OPEN, kernel)
    #cv2.imshow('openingimg', openingimg)
    #cv2.waitKey(0)
    #cv2.imwrite('openingimg.jpg', openingimg)

    #cv2.imshow('img', img)
    #cv2.waitKey(0)
    #mode取值“CV_RETR_LIST”， 只检测最外围轮廓，包含在外围轮廓内的内围轮廓被忽略
    #method取值“CV_CHAIN_APPROX_SIMPLE”，即检测所有轮廓，但各轮廓之间彼此独立，不建立等级关系，并且仅保存轮廓上拐点信息：
    contours, hierarchy = cv2.findContours(openingimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # 找出最大的三个区域
    blocks = []
    for c in contours:
        #print(c)
        #print(len(contours))
        # 發現有11個輪廓
        # 找出轮廓的左上点和右下点，由此计算它的面积和长宽比
        #r = find_retangle(c)
        y, x = [], []
	
    	for p in c:
            #y = p
            # append() 方法用于在列表末尾添加新的对象。
            #print(p)
            y.append(p[0][0])
            x.append(p[0][1])

        r = [np.min(y), np.min(x), np.max(y), np.max(x)] #頂點
        a = (r[2]-r[0]) * (r[3]-r[1]) #面積 長x寬
        s = (r[2]-r[0]) / (r[3]-r[1]) #長寬比
		
        blocks.append([r, a, s])
            
    blocks = sorted(blocks, key=lambda b: b[2])[-3:]
    
        
    maxweight, maxinedx = 0, -1
    for i in xrange(len(blocks)):
        b = orgimg[blocks[i][0][1]:blocks[i][0][3], blocks[i][0][0]:blocks[i][0][2]]
        #cv2.rectangle(img, (blocks[0][1], blocks[2][1]), (blocks[0][0], blocks[2][0]), (0,255,0),2)
        #cv2.imshow('img', img)
        #cv2.waitKey(0)
        # RGB转HSV
        hsv = cv2.cvtColor(b, cv2.COLOR_BGR2HSV)
        #cv2.imshow('hsv', hsv)
        #cv2.waitKey(0)
		# 蓝色车牌范围
        lower = np.array([100,50,50])
        upper = np.array([140,255,255])
		# 根据阈值构建掩模
        mask = cv2.inRange(hsv, lower, upper)
 
		# 统计权值
        w1 = 0
        for m in mask:
            w1 += m / 255
		
        w2 = 0
        for w in w1:
            w2 += w
			
		# 选出最大权值的区域
        if w2 > maxweight:
            maxindex = i
            maxweight = w2
    #cv2.imwrite('img.jpg', img)
    rect = blocks[maxindex][0]

    cv2.rectangle(img, (rect[0], rect[1]), (rect[2], rect[3]), (0,255,0),2)
    cv2.imshow('img', img)
 
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    ax1.imshow(img11, plt.cm.gray, interpolation='nearest')
    ax1.axis('off')
    ax2.imshow(img,interpolation='nearest')
    ax2.axis('off')         
    fig.tight_layout()
    plt.show()  

