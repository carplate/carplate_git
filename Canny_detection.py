#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 15:22:16 2019

@author: scott
"""
from skimage import img_as_ubyte
from skimage import measure,color
from skimage import io,data

import cv2
import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    '''Step1: 輸入一張圖'''
    original_image = cv2.imread('in_H.jpg')
    original_image = cv2.resize(original_image, (400, 400*original_image.shape[0]/original_image.shape[1])) # 等比例縮小
    '''Step2: 抓出rgb'''
    R = original_image[:,:,0]
    G = original_image[:,:,1]
    B = original_image[:,:,2]
    #cv2.imshow('R', R)
    #cv2.waitKey(0)
    '''Step3: 利用方程式把image轉成灰階'''
    Gray_image = R*0.299 + G*0.587 + B*0.114 
    '''把他scale到0~1'''
    Gray_image_scale = Gray_image /255
    #cv2.imshow('Gray_image_scale', Gray_image_scale)
    #cv2.waitKey(0)
    Gray_image_Height = Gray_image.shape[0]
    Gray_image_Width = Gray_image.shape[1]
    '''Step4: 畫出histogram'''
    histogram = np.zeros(256)
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            histogram[int(Gray_image[i][j])] = histogram[int(Gray_image[i][j])]+1

    """
    bins = np.arange(0, 256, 1) 
    k = plt.bar(bins, histogram, .5)
    plt.title('(0.299*R)+(0.587*G)+(0.114*B) histogram')
    plt.xlabel('Gray scale')
    plt.show()
    """
    
    '''Step5: histogram equalisation'''
    dst=img_as_ubyte(Gray_image/255)
    Gray_image_histogram_equalisation = cv2.equalizeHist(dst)
    
    '''Step5-1: 畫出histogram'''
    histogram_after = np.zeros(256)
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            histogram_after[int(Gray_image_histogram_equalisation[i][j])] = histogram_after[int(Gray_image_histogram_equalisation[i][j])]+1
    """
    bins = np.arange(0, 256, 1) 
    k = plt.bar(bins, histogram_after, .5)
    plt.title('(0.299*R)+(0.587*G)+(0.114*B) histogram equalisation')
    plt.xlabel('Gray scale')
    plt.show()
    """
    
    '''Step5-2: 畫出對照圖'''
    """
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4))
    ax1.imshow(dst, plt.cm.gray, interpolation='nearest')
    ax1.axis('off')
    ax2.imshow(Gray_image_histogram_equalisation, plt.cm.gray, interpolation='nearest')
    ax2.axis('off')         
    fig.tight_layout()
    plt.show()  
    """

    '''Step6: 用sobel做邊緣偵測'''
    
    
    blurred = cv2.GaussianBlur(Gray_image_histogram_equalisation, (7, 7), 0)

    canny = cv2.Canny(blurred, 200, 300)
    dst=io.imshow(canny)
    io.show()
    
    '''Step7: 把sobel image做二值化'''
    ImageThreshold = np.zeros([Gray_image_Height, Gray_image_Width])
    for i in range(Gray_image_Height):
        for j in range(Gray_image_Width):
            if canny[i][j] <= 127:
                ImageThreshold[i][j] = 0
            else:
                ImageThreshold[i][j] = 255

    ImageThreshold=img_as_ubyte(ImageThreshold/255)
    dst=io.imshow(ImageThreshold)
    io.show()
    
    
    '''Step8: 開運算'''
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
    openingimg = cv2.morphologyEx(ImageThreshold, cv2.MORPH_OPEN, element2)       
    dst=io.imshow(openingimg)
    io.show()  
    '''Step8-1: 閉運算'''
    element3 = cv2.getStructuringElement(cv2.MORPH_RECT, (6, 6))     
    closingimg = cv2.morphologyEx(openingimg, cv2.MORPH_CLOSE, element3)
    dst=io.imshow(closingimg)
    io.show()   