#-*-coding: utf-8-*-  

import cv2
import numpy as np
 
 
# 形态学处理
def Process(img):
	# 高斯平滑
	gaussian = cv2.GaussianBlur(img, (3, 3), 0, 0, cv2.BORDER_DEFAULT)
    #cv2.imshow('img', img)
    #cv2.waitKey(0)
	# 中值滤波
    # 圖片上經常會看到有一點一點雜訊這種雜訊稱之為胡椒鹽雜訊(Salt and pepper noise)，這種雜訊我們可以利用中值濾波器將它給濾除，讓圖片比較接近原始的狀態。
	median = cv2.medianBlur(gaussian, 5)
	# Sobel算子
    # Sobel是一種獲得影像一階梯度的手法，常見應用於邊緣檢測
	# 梯度方向: x
	sobel = cv2.Sobel(median, cv2.CV_8U, 1, 0, ksize=3)
	# 二值化
	ret, binary = cv2.threshold(sobel, 170, 255, cv2.THRESH_BINARY)
	# 核函数
    # OpenCV提供getStructuringElement()讓我們得到要進行侵蝕或膨脹的模板
	element1 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
	element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 7))
	# 膨胀
	dilation = cv2.dilate(binary, element2, iterations=1)
	# 腐蚀
	erosion = cv2.erode(dilation, element1, iterations=1)
	# 膨胀
	dilation2 = cv2.dilate(erosion, element2, iterations=3)
	return dilation2
 
 
def GetRegion(img):
	regions = []
	# 查找轮廓
	contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	for contour in contours:
		area = cv2.contourArea(contour)
		if (area < 2000):
			continue
		eps = 1e-3 * cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, eps, True)
		rect = cv2.minAreaRect(contour)
		box = cv2.boxPoints(rect)
		box = np.int0(box)
		height = abs(box[0][1] - box[2][1])
		width = abs(box[0][0] - box[2][0])
		ratio =float(width) / float(height)
		if (ratio < 5 and ratio > 1.8):
			regions.append(box)
	return regions
 
 
def detect(img):
	# 灰度化
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    prc = Process(gray)
	
    #regions = []    
    #contours, hierarchy = cv2.findContours(prc, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
	#regions = GetRegion(prc)
	#print('[INFO]:Detect %d license plates' % len(regions))
	#for box in regions:
	#	cv2.drawContours(img, [box], 0, (0, 255, 0), 2)
    #cv2.imshow('Result', prc)
    #保存结果文件名
	#cv2.imwrite('result2.jpg', img)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    return prc
 
if __name__ == '__main__':
    #输入的参数为图片的路径
    img = cv2.imread('in3.jpg')
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #cv2.imshow('gray', gray)
    #cv2.waitKey(0)
    gaussian = cv2.GaussianBlur(img, (3, 3), 0, 0, cv2.BORDER_DEFAULT)
    #cv2.imshow('gaussian', gaussian)
    #cv2.waitKey(0)
    median = cv2.medianBlur(gaussian, 5)
    #cv2.imshow('median', median)
    #cv2.waitKey(0)
    sobel = cv2.Sobel(median, cv2.CV_8U, 1, 0, ksize=3)
    #cv2.imshow('sobel', sobel)
    #cv2.waitKey(0)
    ret, binary = cv2.threshold(sobel, 170, 255, cv2.THRESH_BINARY)
    #cv2.imshow('binary', binary)
    #cv2.waitKey(0)
    element1 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 1))
    element2 = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 7))
    #prc = Process(gray)
    
    #prc = Process(gray)
    
	
    regions = []    
    contours, hierarchy = cv2.findContours(prc, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for contour in contours:
        area = cv2.contourArea(contour)  #計算面積
        if (area < 2000):
            continue
        eps = 1e-3 * cv2.arcLength(contour, True) #計算周長
        approx = cv2.approxPolyDP(contour, eps, True)
        rect = cv2.minAreaRect(contour) #得到最小外接矩形的（中心(x,y), (宽,高), 旋转角度）
        box = cv2.cv.BoxPoints(rect) #获取最小外接矩形的4个顶点坐标
        box = np.int0(box) #np.int0 可以用来省略小数点后面的数字（非四捨五入）。
        height = abs(box[0][1] - box[2][1])
        width = abs(box[0][0] - box[2][0])
        ratio = float(width) / float(height)
        if (ratio < 5 and ratio > 1.8):
            regions.append(box)
        
    for box1 in regions:
        cv2.drawContours(img, [box1], 0, (0, 255, 0), 2)
    
    #cv2.imshow("Image", img) 
    #cv2.waitKey (0)
    #cv2.destroyAllWindows()
        
        
        
        
        
        
        
        
        
        
        
        
        
        